# CalcuCo Calculator Frontend

The application consumes the APIs of Calculator and gives the User Interface for the following operations add, subtract, divide, multiple, factorial, power, square root and cuberoot.

## Getting Started

The application is developed using the Vanila JavaScript Prototyping approach.To get started, Clone the project on your machine for development and testing purposes and simply serve the web page to run it. You may need to enable the CORS for your port and domain.

### Prerequisites

Install the git on your machine
```
git
```
### Installing

Clone the project using the command below.

```
$ git clone https://bitbucket.org/seabdulsamad/calculator-frontend.git
```

## Authors

* **Abdul Samad** - se.abdulsamad@gmail.com

## License

This project is licensed under the MIT License [Abdul Samad](http://linkedin.com/in/seabdulsamad)