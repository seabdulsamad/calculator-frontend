$(document).ready(function(){
	var calculator = {
		operand_x : null,
		operand_y : null,
		operator : null,
		init:function(){
			var that = this;
			that.operand_x = null;
			that.operand_y = null;
			that.operator = null;
			$('#field').val('');
			$('#result_continer').html('');
			this.populateButtons();
		},
		onButtonPress:function(el){
			var that = this;
			var button = el.currentTarget.value;
			if( button=='c'){
				that.init();
			}if(button == '='){
				console.log('REQUEST GENERATED');
				that.sendRequest();
			}else{
				that.operator = button;
				$('#field').val('').focus();
			}
		},
		sendRequest:function(){
			var that = this;
			var formData = {
				operand_x:that.operand_x,
				operand_y:that.operand_y,
				operator:that.operator
			};
			$.ajaxSetup({
			    crossDomain: true,
			});
			$.post('http://localhost:8000/calculator',formData,function(data){
				var result = '';
				$.each(data,function(key,val){
					result += key + ': ' + val + '<br/>';
				});
				$('#result_continer').html(result);
			},'json');
		},
		populateButtons:function(){
			var that = this;
			var default_disabled = ['add','subtract','multiple','divide','sqrt','qubrt','power','factorial','='];
			var buttons = ['c','add','subtract','multiple','divide','sqrt','qubrt','power','factorial','='];
			var buttonsEl='';
			$.each(buttons,function(key,val){
				if(key==4){
					buttonsEl+='<br/><br/>';
				}
				var disabled = '';
				if($.inArray(val , default_disabled)!==-1){
					disabled = 'disabled';
				}else{
					console.log(val);
				}
				buttonsEl +='<button class="btn btn-primary" value="'+ val +'" style="text-transform:uppercase;padding:2px 8px;" '+ disabled +'>'+ val +'</button>';
			});
			$('#button_container').html(buttonsEl);
			$('#field').on('keyup',function(e){
				e.preventDefault();
				var operand = e.currentTarget.value;
				if(operand!= null){
					if(that.operand_x == null || that.operator == null){
						that.operand_x = operand;
					}else{
						that.operand_y = operand;
					}
					that.enableAllButtons();
				}
			});
		},
		enableAllButtons:function(){
			var that = this;
			$('#button_container button').removeAttr('disabled');
			$('#button_container button').click(function(e){
				e.preventDefault();
				that.onButtonPress(e);
			});
		}
	};
	calculator.init();
});